<?php

/**
 * Smile Contact appeal repository.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

declare(strict_types=1);

namespace Smile\Contact\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Smile\Contact\Api\AppealRepositoryInterface;
use Smile\Contact\Api\Data;
use Smile\Contact\Api\Data\AppealInterface;
use Smile\Contact\Api\Data\AppealSearchResultsInterface;
use Smile\Contact\Model\Appeal;
use Smile\Contact\Model\AppealFactory;
use Smile\Contact\Model\ResourceModel\Appeal as ResourceAppeal;
use Smile\Contact\Model\ResourceModel\Appeal\CollectionFactory as AppealCollectionFactory;

/**
 * Class CommentRepository
 *
 * @package Smile\Contact\Model\AppealRepository
 */
class AppealRepository implements AppealRepositoryInterface
{
    /**
     * Appeal collection factory.
     *
     * @var AppealCollectionFactory
     */
    protected $appealCollectionFactory;

    /**
     * Appeal factory.
     *
     * @var AppealFacotry
     */
    protected $appealFactory;

    /**
     * Resource appeal.
     *
     * @var ResourceAppeal
     */
    protected $resource;

    /**
     * Appeal search results interface factory.
     *
     * @var AppealSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * Appeal Repository constructor.
     *
     * @param AppealCollectionFactory                  $appealCollectionFactory
     * @param AppealFactory                            $appealFactory
     * @param Data\AppealSearchResultsInterfaceFactory $searchResultsFactory
     * @param ResourceAppeal                           $resource
     */
    public function __construct(
        AppealCollectionFactory $appealCollectionFactory,
        AppealFactory $appealFactory,
        Data\AppealSearchResultsInterfaceFactory $searchResultsFactory,
        ResourceAppeal $resource
    ) {
        $this->appealCollectionFactory = $appealCollectionFactory;
        $this->appealFactory = $appealFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->resource = $resource;
    }

    /**
     * Save appeal data.
     *
     * @param AppealInterface $appeal
     *
     * @return AppealInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(AppealInterface $appeal): AppealInterface
    {
        try {
            $this->resource->save($appeal);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $appeal;
    }

    /**
     * Load appeal data by given appeal Identity.
     *
     * @param int $appaelId
     *
     * @return AppealInterface
     *
     * @throws NoSuchEntityException
     */
    public function getById(int $appaelId): AppealInterface
    {
        /* @var Appeal $appeal */
        $appeal = $this->appealFactory->create();
        $this->resource->load($appeal, $appaelId);
        if (!$appeal->getId()) {
            throw new NoSuchEntityException(__('Appeal with id "%1" does not exist.', $appaelId));
        }

        return $appeal;
    }

    /**
     * Load appeal data collection by given search criteria.
     *
     * @param SearchCriteriaInterface $criteria
     *
     * @return AppealSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria = null): AppealSearchResultsInterface
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $collection = $this->appealCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $appeal = [];
        /** @var AppealInterface $appealModel */
        foreach ($collection as $appealModel) {
            $appeal[] = $appealModel;
        }
        $searchResults->setItems($appeal);

        return $searchResults;
    }

    /**
     * Delete Appeal.
     *
     * @param AppealInterface $appeal
     *
     * @return bool
     *
     * @throws CouldNotDeleteException
     */
    public function delete(AppealInterface $appeal): bool
    {
        try {
            $this->resource->delete($appeal);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete appeal by given appeal Identity.
     *
     * @param int $appealId
     *
     * @return bool
     *
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $appealId): bool
    {
        return $this->delete($this->getById($appealId));
    }
}
