<?php
/**
 * Smile Contact appeal.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

declare(strict_types=1);

namespace Smile\Contact\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Appeal
 *
 * @package Smile\Contact\Model\ResourceModel\Appeal
 */
class Appeal extends AbstractDb
{
    /**
     * Initialize resource model.
     *
     * @return void
     */
    public function _construct(): void
    {
        $this->_init('smile_contact', 'id');
    }
}
