<?php

/**
 * Smile Сontact send email model.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

declare(strict_types=1);

namespace Smile\Contact\Model;

use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use phpDocumentor\Reflection\Types\Self_;
use Psr\Log\LoggerInterface;
use Smile\Contact\Api\SendEmailInterface;

class SendEmail implements SendEmailInterface
{
    /**
     * Email template.
     */
    const EMAIL_TEMPLATE = 'appeal_email_email_template';

    /**
     *  Sender email.
     */
    const SENDER_EMAIL = 'trans_email/ident_general/email';

    /**
     *  Sender name.
     */
    const SENDER_NAME = 'trans_email/ident_general/name';

    /**
     * Scope Config Interface.
     *
     * @var ScopeConfigInterface
     */
    protected $contactsConfig;

    /**
     * TransportBuilder.
     *
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * State Interface.
     *
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * Store Manager Interface.
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Logger Interface.
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Initialize dependencies.
     *
     * @param ScopeConfigInterface $contactsConfig
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface|null $storeManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        ScopeConfigInterface $contactsConfig,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        LoggerInterface $logger,
        StoreManagerInterface $storeManager = null
    ) {
        $this->contactsConfig = $contactsConfig;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->logger = $logger;
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
    }

    /**
     * Send email for appeal.
     *
     * @param string $email
     * @param array  $data
     *
     * @return bool
     * @throws LocalizedException
    */
    public function send($email, $data): bool
    {
        $this->inlineTranslation->suspend();
        $storeId = $this->storeManager->getStore()->getId();

        $vars = [
            'name' => $data['name'],
            'answer' => $data['answer'],
            'comment' => $data['comment'],
            'store' => $this->storeManager->getStore()
        ];

        $sender['name'] = $this->contactsConfig->getValue(
            self::SENDER_NAME,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $sender['email'] = $this->contactsConfig->getValue(
            self::SENDER_EMAIL,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        $transport = $this->transportBuilder->setTemplateIdentifier(self::EMAIL_TEMPLATE)
            ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => $storeId])
            ->setTemplateVars($vars)
            ->setFromByScope($sender)
            ->addTo($email)
            ->getTransport();

        try {
            $transport->sendMessage();
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }
        $this->inlineTranslation->resume();

        return true;
    }
}
