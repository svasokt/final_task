<?php

/**
 * Smile Contact.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

declare(strict_types=1);

namespace Smile\Contact\Plugin\Controller\Index;

use Magento\Contact\Controller\Index\Post as ContactPost;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Psr\Log\LoggerInterface;
use Smile\Contact\Api\AppealRepositoryInterface;
use Smile\Contact\Model\AppealFactory;

/**
 * Class PostPlugin
 *
 * @package Smile\Contact\Plugin\Controller\Index
 */
class PostPlugin
{
    /**
     * Appeal repository interface.
     *
     * @var AppealRepositoryInterface
     */
    protected $appealRepository;

    /**
     * Appeal factory.
     *
     * @var AppealFactory
     */
    protected $appealFactory;

    /**
     * Logger.
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * PostPlugin constructor.
     *
     * @param Context                   $context
     * @param AppealRepositoryInterface $appealRepository
     * @param AppealFactory             $appealFactory
     * @param LoggerInterface           $logger
     */
    public function __construct(
        Context                   $context,
        AppealRepositoryInterface $appealRepository,
        AppealFactory             $appealFactory,
        LoggerInterface           $logger
    ){
        $this->appealRepository = $appealRepository;
        $this->appealFactory = $appealFactory;
        $this->logger = $logger;
    }
    /**
     * After save in db.
     * Save data with contact us in db.
     *
     * @param ContactPost $subject
     * @param Redirect $result
     *
     * @return Redirect
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterExecute(ContactPost $subject, $result): Redirect
    {
        $data = $subject->getRequest()->getPostValue();

        if ($data) {
            try {
                $appealModel = $this->appealFactory->create();
                $appealModel->setData($data);
                $this->appealRepository->save($appealModel);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }

        return $result;
    }
}
