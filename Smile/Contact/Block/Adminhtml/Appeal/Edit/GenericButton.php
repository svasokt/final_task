<?php

/**
 * Smile Contact generic button block.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

declare(strict_types=1);

namespace Smile\Contact\Block\Adminhtml\Appeal\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Smile\Contact\Api\AppealRepositoryInterface;

/**
 * Class GenericButton
 *
 * @package Smile\Contact\Block\Adminhtml\Appeal\Edit
 */
class GenericButton
{
    /**
     * Context.
     *
     * @var Context
     */
    protected $context;

    /**
     * Appeal repository interface.
     *
     * @var AppealRepositoryInterface
     */
    protected $appealRepository;

    /**
     * GenericButton constructor.
     *
     * @param AppealRepositoryInterface $appealRepository
     * @param Context                   $context
     */
    public function __construct(
        AppealRepositoryInterface $appealRepository,
        Context                   $context
    ) {
        $this->appealRepository = $appealRepository;
        $this->context = $context;
    }

    /**
     * Get Appeal ID.
     *
     * @return int
     */
    public function getAppealId(): int
    {
        try {
            $modelId = $this->context->getRequest()->getParam('id');
            $appealId = $this->appealRepository->getById((int) $modelId)->getAppealId();
        } catch (NoSuchEntityException $e) {
            $this->context->getLogger()->error($e->getMessage());
            $appealId = 0;
        }

        return $appealId;
    }

    /**
     * Generate url by route and parameters.
     *
     * @param   string $route
     * @param   array  $params
     *
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
