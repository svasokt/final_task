<?php

/**
 * Smile Contact save controller.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

declare(strict_types=1);

namespace Smile\Contact\Controller\Adminhtml\Appeal;

use Magento\Backend\App\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Smile\Contact\Api\AppealRepositoryInterface;
use Smile\Contact\Model\Appeal;
use Smile\Contact\Model\AppealFactory;
use Smile\Contact\Api\SendEmailInterface;


/**
 * Class Index
 *
 * @package Smile\Contact\Controller\Adminhtml\Appeal
 */
class Save extends Action
{
    /**
     * Authorization level of a basic admin session.
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Smile_Contact::appeal_save';

    /**
     * Appeal repository interface.
     *
     * @var AppealRepositoryInterface
     */
    protected $appealRepository;


    /**
     * Appeal factory.
     *
     * @var AppealFactory
     */
    protected $appealFactory;

    /**
     * Data persistor interface.
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Send email interface.
     *
     * @var SendEmailInterface
     */
    protected $mail;

    /**
     * Save constructor.
     *
     * @param Action\Context             $context
     * @param AppealFactory              $appealFactory
     * @param AppealRepositoryInterface  $appealRepository
     * @param DataPersistorInterface     $dataPersistor
     * @param SendEmailInterface              $mail
     */
    public function __construct(
        Action\Context $context,
        AppealFactory             $appealFactory,
        AppealRepositoryInterface $appealRepository,
        DataPersistorInterface    $dataPersistor,
        SendEmailInterface             $mail
    ) {
        parent::__construct($context);
        $this->appealFactory = $appealFactory;
        $this->appealRepository = $appealRepository;
        $this->dataPersistor = $dataPersistor;
        $this->mail = $mail;
    }

    /**
     * Save action.
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $id = $data['id'];
            $email = $data['email'];
            $answer = $data['answer'];

            try {
                $appealModel = $this->appealRepository->getById((int)$id);

                $data['status'] = Appeal::STATUS_ANSWERED;
                $appealModel->setData($data);
                $this->appealRepository->save($appealModel);

                if ($email && $answer) {
                    $this->mail->send($data['email'], $data);
                }

                $this->_eventManager->dispatch(
                    'smile_contact_appeal_save_before',
                    ['appeal' => $appealModel]
                );

                $this->messageManager->addSuccessMessage(__('You saved and sent the appeal.'));
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while save or send the appeal.'));
                $this->dataPersistor->set('smile_contact_appeal', $data);

                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }

        return $resultRedirect->setPath('*/*/');
    }
}
