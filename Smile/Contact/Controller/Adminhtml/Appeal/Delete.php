<?php

/**
 * Smile Contact delete controller.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

declare(strict_types=1);

namespace Smile\Contact\Controller\Adminhtml\Appeal;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Smile\Contact\Api\AppealRepositoryInterface;

/**
 * Class Delete
 *
 * @package Smile\Contact\Controller\Adminhtml\Appeal
 */
class Delete extends Action
{
    /**
     * Authorization level of a basic admin session.
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Smile_Contact::appeal_delete';

    /**
     * Appeal repository interface.
     *
     * @var AppealRepositoryInterface
     */
    protected $appealRepository;

    /**
     * Delete constructor.
     *
     * @param Action\Context            $context
     * @param AppealRepositoryInterface $appealRepository
     */
    public function __construct(
        Action\Context            $context,
        AppealRepositoryInterface $appealRepository
    ) {
        $this->appealRepository = $appealRepository;
        parent::__construct($context);
    }

    /**
     * Delete action.
     *
     * @return Redirect
     */
    public function execute(): Redirect
    {
        $id = $this->getRequest()->getParam('id');
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $result = $resultRedirect->setPath('*/*/');

        if ($id) {
            try {
                $this->appealRepository->deleteById((int) $id);
                $this->messageManager->addSuccessMessage(__('The appeal has been deleted.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $result = $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }

        return $result;
    }
}
