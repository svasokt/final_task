<?php

/**
 * Smile Contact Send email interface.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

declare(strict_types=1);

namespace Smile\Contact\Api;

/**
 * Interface AppealRepositoryInterface
 *
 * @package Smile\Contact\Api
 */
interface SendEmailInterface
{
    /**
     * Send email for appeal.
     *
     * @param string $email
     * @param array $data
     *
     * @return bool
     */
    public function send($email, $data): bool;
}
