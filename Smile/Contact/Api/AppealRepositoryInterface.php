<?php

/**
 * Smile Contact Appeal repository interface.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

declare(strict_types=1);

namespace Smile\Contact\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Smile\Contact\Api\Data\AppealInterface;
use Smile\Contact\Api\Data\AppealSearchResultsInterface;

/**
 * Interface AppealRepositoryInterface
 *
 * @package Smile\Contact\Api
 */
interface AppealRepositoryInterface
{
    /**
     * Retrieve a appeal by it's id.
     *
     * @param int $appealId
     *
     * @return AppealInterface
     *
     * @throws NoSuchEntityException
     */
    public function getById(int $appealId): AppealInterface;

    /**
     * Retrieve appeal which match a specified criteria.
     *
     * @param SearchCriteriaInterface|null $searchCriteria
     *
     * @return AppealSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null): AppealSearchResultsInterface;

    /**
     * Save appeal.
     *
     * @param AppealInterface $appeal
     *
     * @return AppealInterface
     *
     * @throws CouldNotSaveException
     */
    public function save(AppealInterface $appeal): AppealInterface;

    /**
     * Delete a appeal by its id
     *
     * @param int $appealId
     *
     * @return bool
     *
     * @throws NoSuchEntityException
     */
    public function deleteById(int $appealId): bool;
}
