<?php

/**
 * Smile Contact Appeal search results interface.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

declare(strict_types=1);

namespace Smile\Contact\Api\Data;

/**
 * Interface AppealSearchResultsInterface
 *
 * @package Smile\Contact\Api\Data
 */
interface AppealSearchResultsInterface
{
    /**
     * Get appeal list.
     *
     * @return \Smile\Contact\Api\Data\AppealInterface[]
     */
    public function getItems(): array;

    /**
     * Set appeal list.
     *
     * @return \Smile\Contact\Api\Data\AppealInterface[] $items
     *
     * @return AppealSearchResultsInterface
     */
    public function setItems(array $items): AppealSearchResultsInterface;
}
