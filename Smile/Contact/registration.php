<?php

/**
 * Smile Contact.
 *
 * @author    Vasyl Sorivka <vasyl.sorivka@smile-ukraine.com>
 * @copyright 2020 Smile
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Smile_Contact', __DIR__);
